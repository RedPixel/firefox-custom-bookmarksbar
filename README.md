# firefox-custom-bookmarksbar

A custom userChrome.css to hide text from bookmarks at the bookmarks bar in Firefox. Since the new extensions engine since Firefox 57 does not allow extensions to modify the look of Firefox anymore. This is a quick fix for which I used to use the Roomy Bookmarks Bar extension.

This file goes to `C:\Users\<USERNAME>\AppData\Roaming\Mozilla\Firefox\Profiles\<CURRENT_PROFILE>.default\chrome\userChrome.css`.
If you don't have the `chrome` folder, you need to create it.

By default, userChrome.css modifications are disabled in Firefox. You need to make sure that on the about:config page in Firefox, the toolkit.legacyUserProfileCustomizations.stylesheets preference is set to true and then restart the browser. 
